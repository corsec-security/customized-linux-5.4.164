#include<stdio.h>
#include<dirent.h>
#include<string.h>
#include<openssl/hmac.h>
#include<openssl/err.h>
#include<sys/stat.h>
#include<sys/types.h>


#define FIPS_INTEGRITY_HMAC_KEY_LEN ( 32 )
#define FIPS_INTEGRITY_HMAC_BYTELEN ( 32 )
#define FIPS_INTEGRITY_HMAC_BUFLEN ( 2 * FIPS_INTEGRITY_HMAC_BYTELEN )
#define FIPS_INTEGRITY_READBUF_LEN ( 4096 )
#define PATH_TO_HMAC_FILE "kernel_hmacs"

//unlikely to need more than half a KB for a single line of the HMAC file
#define HMAC_LINE_BUF_SIZE (512) 

static const char fips_integrity_key[] = {
        'o','r','b','o','D','e','J','I',
        'T','I','T','e','j','s','i','r',
        'p','A','D','O','N','i','v','i',
        'r','p','U','k','v','a','r','P'
};

static unsigned char * hmac_to_hex(unsigned char *in){
    int i;
    unsigned char *out=calloc(FIPS_INTEGRITY_HMAC_BUFLEN, 1);
    static const char hexchars[] = "0123456789abcdef";
    for(i=0;i<FIPS_INTEGRITY_HMAC_BYTELEN;i++){
        out[2*i]=hexchars[*(in+i)>>4];
        out[2*i+1]=hexchars[*(in+i) & 0x0f];
    }
    return out;
}

int run_one_hmac(char *path2file, char *known_hmac){
    unsigned int len;
    size_t read_bytes;
    ssize_t i;
    off_t sourcefilelen;
    struct stat st;
    FILE *sourcefile;
    unsigned char *readbuf=NULL;
    unsigned char *hexhmac=NULL;
    unsigned char calculated_hmac[FIPS_INTEGRITY_HMAC_BYTELEN];
    HMAC_CTX* hmac;

    int rv=-1;

    const EVP_MD *md = EVP_sha256();


    //used in case a \n is included
    //use the buflen to get last position because of 0-indexing
    known_hmac[FIPS_INTEGRITY_HMAC_BUFLEN]='\0';

    sourcefile=fopen(path2file, "rb");
    if(!sourcefile){
        // printf("err opening sourcefile\n");
        goto done;
    }

    readbuf=malloc(FIPS_INTEGRITY_READBUF_LEN);
    if(!readbuf){
        // printf("failed to malloc readbuf\n");
        goto done;
    }

    hmac=HMAC_CTX_new();
    if(!hmac){
        // printf("failed to alloc hmac struct\n");
        goto done;
    }
    if(!HMAC_Init_ex(hmac, fips_integrity_key, FIPS_INTEGRITY_HMAC_KEY_LEN, md, NULL)){
        // printf("error with hmac initialization\n");
        // ERR_print_errors_fp(stderr);
        goto done;
    }
    
    i=0;
    while(!feof(sourcefile)){
        read_bytes=fread(readbuf, sizeof(unsigned char), FIPS_INTEGRITY_READBUF_LEN, sourcefile);
        i+=read_bytes;
        if(ferror(sourcefile)){
            // printf("error while while reading sourcefile at position %ld\n", i);
            goto done;
        }

        if(!HMAC_Update(hmac, readbuf, read_bytes)){
            goto done;
        }
    }
    if(!HMAC_Final(hmac, calculated_hmac, &len)){
        goto done;
    }
    hexhmac=hmac_to_hex(calculated_hmac);

    rv=memcmp(known_hmac, hexhmac, FIPS_INTEGRITY_HMAC_BUFLEN);

done:
    HMAC_CTX_free(hmac);
    if(sourcefile)
        fclose(sourcefile);
    
    OPENSSL_clear_free(hexhmac, FIPS_INTEGRITY_HMAC_BUFLEN);
    OPENSSL_clear_free(readbuf, FIPS_INTEGRITY_READBUF_LEN);
    return rv;


}


int main(void){
    char hmac_linebuf[HMAC_LINE_BUF_SIZE];
    FILE *hmacfile;
    int rv;
    int numfiles;
    int i;

    memset(hmac_linebuf, 0, sizeof(hmac_linebuf));

    hmacfile=fopen(PATH_TO_HMAC_FILE, "r");
    if(!hmacfile){
        return -1;
    }
    if( !fgets(hmac_linebuf, sizeof(hmac_linebuf), hmacfile)){
        // printf("error reading file count\n");
        rv=-1;
        goto done; 
    }
    if( !(numfiles = atoi(hmac_linebuf)) ) {
        // printf("could not parse file count\n");
        rv = -1;
        goto done;
    }
    for(i=0;i<numfiles;i++){

        if(!fgets(hmac_linebuf, sizeof(hmac_linebuf), hmacfile)){
            // printf("error in fgets\n");
            rv=-1;
            goto done;
        }
        hmac_linebuf[FIPS_INTEGRITY_HMAC_BUFLEN]='\0';
        //remove the \n from the filename
        hmac_linebuf[FIPS_INTEGRITY_HMAC_BUFLEN+strlen(hmac_linebuf+FIPS_INTEGRITY_HMAC_BUFLEN+1)]='\0';
        // printf("hmac: %s\n", hmac_linebuf);
        // printf("file: %s\n", hmac_linebuf+FIPS_INTEGRITY_HMAC_BUFLEN+1);
        rv=run_one_hmac(hmac_linebuf+FIPS_INTEGRITY_HMAC_BUFLEN+1, hmac_linebuf );
        if(rv){
            goto done;
        }
    }
    
done:
    if(hmacfile)
        fclose(hmacfile);
    
    memset(hmac_linebuf, 0, sizeof(hmac_linebuf));

    return rv;

}